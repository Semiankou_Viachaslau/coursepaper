var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;

server.listen(port, function () {
  console.log('Server listening at port %d', port);
});

app.use(express.static(__dirname + '/'));

console.log("Hello");

var tmp = {};

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

var curPlayers = 4;

var ids = [];

var time = 0;

var map = [
      [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
      [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
      [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
      [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
      [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
      [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
      [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      ];
map = CreateBricks(map);

var players = 0;

var playerPos = [
  0,
  {pos: {x: 0, y: 0}, dead: false},
  {pos: {x: 0, y: 12}, dead: false},
  {pos: {x: 12, y: 0}, dead: false},
  {pos: {x: 12, y: 12}, dead: false}
];

var bombPos = [];
var boomPos = [];

function CreateBricks(a) {
    for( var i=0; i<13; i++ )
    {
        for ( var j=0; j<13; j++ )
        {
            if(!a[i][j])
            {
                var tmp = Math.floor((Math.random() * 3) + 1);
                if(tmp == 2)
                {
                    a[i][j] = 1;
                }
            }
        }
        a[0][0]=a[0][1]=a[1][0]=0;
        a[0][11]=a[0][12]=a[1][12]=0;
        a[11][0]=a[12][0]=a[12][1]=0;
        a[11][12]=a[12][11]=a[12][12]=0;
    }
    return a;
}

function rst (tmp) {
  playerPos = [
    0,
    {pos: {x: 0, y: 0}, dead: false},
    {pos: {x: 0, y: 12}, dead: false},
    {pos: {x: 12, y: 0}, dead: false},
    {pos: {x: 12, y: 12}, dead: false}
  ];
  ids = [];
  map = [
        [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
        [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
        [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
        [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
        [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
        [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
        [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        ];
  map = CreateBricks(map);
  bombPos = [];
  boomPos = [];
  curPlayers = 0;
  return {field:map, pos:playerPos, bombs:bombPos, booms:boomPos, count:curPlayers};
}

var myVar=setInterval(function () {
  for (i = 0; i < bombPos.length; i++)
  {
    bombPos[i].timePlanted += 0.1;
    if( bombPos[i].timePlanted.toFixed(1) == 3.0 )
    {
      map[bombPos[i].y][bombPos[i].x] = 0;
      //-------------------------------------
      data = bombPos[i];
      bombPos.splice(i,1);
      boomPos.push({x:data.x, y:data.y, timeShowed: 0});
      if(data.y < 12)
      {
        if(map[data.y+1][data.x] == 0 || map[data.y+1][data.x] == 3) {
          boomPos.push({x:data.x, y:data.y+1, timeShowed: 0});
          if(data.y+1 < 12)
          {
            if(map[data.y+2][data.x] == 0 || map[data.y+2][data.x] == 3) {
              boomPos.push({x:data.x, y:data.y+2, timeShowed: 0});
            }
            if(map[data.y+2][data.x] == 1) {
              boomPos.push({x:data.x, y:data.y+2, timeShowed: 0});
              map[data.y+2][data.x] = 0;
            }
          }
        }
        if(map[data.y+1][data.x] == 1) {
          boomPos.push({x:data.x, y:data.y+1, timeShowed: 0});
          map[data.y+1][data.x] = 0;
        }
      }
      //------
      if(data.x < 12) {
        if(map[data.y][data.x+1] == 0 || map[data.y][data.x+1] == 3) {
          boomPos.push({x:data.x+1, y:data.y, timeShowed: 0});
          if(data.x+1 < 12) {
            if(map[data.y][data.x+2] == 0 || map[data.y][data.x+2] == 3) {
              boomPos.push({x:data.x+2, y:data.y, timeShowed: 0});
            }
            if(map[data.y][data.x+2] == 1) {
              boomPos.push({x:data.x+2, y:data.y, timeShowed: 0});
              map[data.y][data.x+2] = 0;
            }
          }
        }
        if(map[data.y][data.x+1] == 1) {
          boomPos.push({x:data.x+1, y:data.y, timeShowed: 0});
          map[data.y][data.x+1] = 0;
        }
      }
      //------
      if(data.x > 0) {
        if(map[data.y][data.x-1] == 0 || map[data.y][data.x-1] == 3) {
          boomPos.push({x:data.x-1, y:data.y, timeShowed: 0});
          if(data.x-1 > 0) {
            if(map[data.y][data.x-2] == 0 || map[data.y][data.x-2] == 3) {
              boomPos.push({x:data.x-2, y:data.y, timeShowed: 0});
            }
            if(map[data.y][data.x-2] == 1) {
              boomPos.push({x:data.x-2, y:data.y, timeShowed: 0});
              map[data.y][data.x-2] = 0;
            }
          }
        }
        if(map[data.y][data.x-1] == 1 && data.x > 0) {
          boomPos.push({x:data.x-1, y:data.y, timeShowed: 0});
          map[data.y][data.x-1] = 0;
        }
      }
      //------
      if(data.y > 0) {
        if(map[data.y-1][data.x] == 0 || map[data.y-1][data.x] == 3) {
          boomPos.push({x:data.x, y:data.y-1, timeShowed: 0});
          if(data.y-1 > 0) {
            if(map[data.y-2][data.x] == 0 || map[data.y-2][data.x] == 3) {
              boomPos.push({x:data.x, y:data.y-2, timeShowed: 0});
            }
            if(map[data.y-2][data.x] == 1) {
              boomPos.push({x:data.x, y:data.y-2, timeShowed: 0});
              map[data.y-2][data.x] = 0;
            }
          }
        }
        if(map[data.y-1][data.x] == 1) {
          boomPos.push({x:data.x, y:data.y-1, timeShowed: 0});
          map[data.y-1][data.x] = 0;
        }
      }
      tmp.booms = boomPos;
      tmp.field = map;
      //-----------
      for(var i=1; i<5; i++)
      {
        for(var j=0; j<boomPos.length; j++)
        {
          if(boomPos[j].x == playerPos[i].pos.x && boomPos[j].y == playerPos[i].pos.y)
          {
            playerPos[i].dead = true;
            tmp.pos = playerPos;
            io.sockets.emit('hit', i);
          }
        }
      }
      //---------------
    }
  }
  for (i = 0; i < boomPos.length; i++)
  {
    boomPos[i].timeShowed += 0.1;
    if( boomPos[i].timeShowed.toFixed(1) == 0.4 )
    {
      boomPos.splice(i,1);
    }
  }
}, 100);

io.sockets.on('connection', function (socket) {
    uuid = guid();
    players++;
    tmp = {field:map, pos:playerPos, bombs:bombPos, booms:boomPos, count:curPlayers};
    socket.emit('start',uuid);
    socket.emit('signal', tmp);
    socket.emit('getBombs', tmp.bombs);
    socket.on('checkID', function (data) {
      if(ids.length==3){
        for(var i=0;i<4;i++)
        {
          if(ids[i]===data){
            socket.emit('setID',i+1)
            return;
          }
        }
      }
      else {
        for(var i=0;i<ids.length;i++)
        {
          if(ids[i]===data){
            socket.emit('setID',i+1)
            return;
          }
        }
        ids.push(data);
        socket.emit('setID',ids.length);
        curPlayers=ids.length;
        tmp.count=curPlayers;
      }
    });
    socket.on('rebuilt', function (data) {
      map = data;
      tmp.field = map;
      socket.emit('signal', tmp);
    });
    socket.on('moveRight', function (id) {
      if(playerPos[id].pos.x<12 && map[playerPos[id].pos.y][playerPos[id].pos.x+1]==0) {
        playerPos[id].pos.x++;
        socket.emit('signal', tmp);
      }
    });
    socket.on('moveLeft', function (id) {
      if(playerPos[id].pos.x>0 && map[playerPos[id].pos.y][playerPos[id].pos.x-1]==0) {
        playerPos[id].pos.x--;
        socket.emit('signal', tmp);
      }
    });
    socket.on('moveUp', function (id) {
      if(playerPos[id].pos.y>0 && map[playerPos[id].pos.y-1][playerPos[id].pos.x]==0) {
        playerPos[id].pos.y--;
        socket.emit('signal', tmp);
      }
    });
    socket.on('moveDown', function (id) {
      if(playerPos[id].pos.y<12 && map[playerPos[id].pos.y+1][playerPos[id].pos.x]==0) {
        playerPos[id].pos.y++;
        socket.emit('signal', tmp);
      }
    });
    socket.on('death', function (id) {
      playerPos[id].pos.x = -1;
      playerPos[id].pos.y = -1;
      playerPos[id].dead = true;
      socket.emit('signal', tmp);
    });
    socket.on('plantBomb', function (id) {
      bombPos.push({x:playerPos[id].pos.x, y:playerPos[id].pos.y, timePlanted: 0});
      map[playerPos[id].pos.y][playerPos[id].pos.x] = 3;
      tmp.map = map;
      tmp.bombs = bombPos;
      socket.emit('signal', tmp);
    });
    socket.on('updateBombs', function (data) {
      bombPos = data;
      tmp.bombs = bombPos;
      socket.emit('getBombs', tmp.bombs);
      socket.emit('signal', tmp);
    });
    socket.on('reset', function (data) {
      tmp = rst();
      socket.emit('getBombs', tmp.bombs);
      socket.emit('signal', tmp);
    });
    socket.on('updateBooms', function (data) {
      boomPos = data;
      tmp.booms = boomPos;
      socket.emit('signal', tmp);
    });
    socket.on('getBoom', function (data) {
      socket.emit('signal', tmp);
      socket.emit('getBooms', tmp.booms);
    });
    socket.on('update', function() {
      socket.emit('signal', tmp);
      socket.emit('getBombs', tmp.bombs);
      socket.emit('getBooms', tmp.booms);
    });
});
