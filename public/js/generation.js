var viewport = {
    width  : $(window).width(),
    height : $(window).height()
};

var doc = $(document);

function get_cookie ( cookie_name )
{
  var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

  if ( results )
    return ( unescape ( results[2] ) );
  else
    return null;
}

if( viewport.width <= 1024 )
{
  if(viewport.height > viewport.width)
  {
    cellSize = viewport.width / 9.5;
  }
  else
  {
    cellSize = (viewport.height - 250) / 9.5;
  }
}
else
{
  cellSize = 80;
}
var id;
var example = document.getElementById("example"),
ctx = example.getContext('2d');
var dead = false;
var url = 'http://localhost:3000';
var socket = io();
var map = [
      [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
      [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
      [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
      [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
      [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
      [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
      [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    ];
bombsPos = [];
boomsPos = [];
number = 0;
curPos = [{dead:false},{dead:false},{dead:false},{dead:false},{dead:false}];
socket.on('start', function (data) {
    id=0;
    if(get_cookie("uuid"))
    {
      console.log(get_cookie("uuid"));
      socket.emit('checkID',get_cookie("uuid"));
    }
    else
    {
      document.cookie = "uuid="+data;
      socket.emit('checkID',get_cookie("uuid"));
      console.log(data);
    }
});
socket.on('setID', function(data){
  id = data;
});
socket.on('signal', function (data) {
  map = data.field;
  console.log(id);
  dead = data.pos[id].dead;
  curPos = data.pos;
  number = data.count;
  Draw();
});
socket.on('getBombs', function (data) {
  bombsPos = data;
  Draw();
});
socket.on('getBooms', function (data) {
  boomsPos = data;
  Draw();
});
socket.on('hit', function (data) {
  if(id == data) alert("You're dead...");
});
example.width = 7.5 * cellSize;
example.height = 7.5 * cellSize;
Draw();

function reset () {
  socket.emit('reset', 0);
}

function Draw(){
  ctx.fillStyle = '#ccc';
  ctx.fillRect(0, 0, example.width, example.height);
  ctx.fillStyle = 'green';
  ctx.fillRect(cellSize/2, cellSize/2, 6.5 * cellSize, 6.5 * cellSize)
  for (var j = 0; j < 13; j++)
  for (var i = 0; i < 13; i++) {
    switch (map[j][i]) {
      case 1:
        DrawBrick(i * cellSize / 2 + cellSize/2, j * cellSize / 2 + cellSize/2);
        break;
      case 2:
        DrawHardBrick(i * cellSize / 2 + cellSize/2, j * cellSize / 2 + cellSize/2);
        break;
      }
  }
  DrawPlayers();
  DrawBomb(bombsPos);
  DrawBoom(boomsPos);
}

function CreateBricks(a) {
    for( var i=0; i<13; i++ )
    {
        for ( var j=0; j<13; j++ )
        {
            if(!a[i][j])
            {
                var tmp = Math.floor((Math.random() * 3) + 1);
                if(tmp == 2)
                {
                    a[i][j] = 1;
                }
            }
        }
        a[0][0]=a[0][1]=a[1][0]=0;
        a[0][11]=a[0][12]=a[1][12]=0;
        a[11][0]=a[12][0]=a[12][1]=0;
        a[11][12]=a[12][11]=a[12][12]=0;
    }
    return a;
}

function DrawBomb(bombPos) {
  for (i = 0; i < bombPos.length; i++) {
      ctx.beginPath();
      ctx.arc((bombPos[i].x+1+1/2)*cellSize/2, (bombPos[i].y+1+1/2)*cellSize/2, cellSize/4, 0, 4 * Math.PI, false);
      ctx.fillStyle = 'yellow';
      ctx.fill();
      ctx.lineWidth = 5;
  }
}

function DrawBoom(boomPos) {
  for (i = 0; i < boomPos.length; i++) {
    DrawSmallBoom(boomPos[i].x,boomPos[i].y)
  }
}

function DrawSmallBoom(x, y) {
  ctx.beginPath();
  ctx.arc((x+1+1/2)*cellSize/2, (y+1+1/2)*cellSize/2, cellSize/4, 0, 2 * Math.PI, false);
  ctx.fillStyle = 'gray';
  ctx.fill();
  ctx.lineWidth = 5;
}

function DrawBrick(x, y) {
    ctx.fillStyle = '#FFA500';
    ctx.fillRect(x, y, cellSize / 2, cellSize / 2);
    ctx.fillStyle = '#CD8500';
    ctx.fillRect(x, y, cellSize / 2, cellSize / 16);
    ctx.fillRect(x, y + cellSize / 4, cellSize / 2, cellSize / 16);
    ctx.fillRect(x + cellSize / 4, y, cellSize / 16, cellSize / 4);
    ctx.fillRect(x + cellSize / 16, y + cellSize / 4, cellSize / 16, cellSize / 4);
    ctx.fillStyle = '#D3D3D3';
    ctx.fillRect(x, y + cellSize / 4 - cellSize / 16, cellSize / 2, cellSize / 16);
    ctx.fillRect(x, y + cellSize / 2 - cellSize / 16, cellSize / 2, cellSize / 16);
    ctx.fillRect(x + cellSize / 4 - cellSize / 16, y, cellSize / 16, cellSize / 4);
    ctx.fillRect(x, y + cellSize / 4 - cellSize / 16, cellSize / 16, cellSize / 4);
}

function DrawHardBrick(x, y) {
    ctx.fillStyle = '#cccccc';
    ctx.fillRect(x, y, cellSize / 2, cellSize / 2);
    ctx.fillStyle = '#909090';
    ctx.beginPath();
    ctx.moveTo(x, y + cellSize / 2);
    ctx.lineTo(x + cellSize / 2, y + cellSize / 2);
    ctx.lineTo(x + cellSize / 2, y);
    ctx.fill();
    ctx.fillStyle = '#eeeeee';
    ctx.fillRect(x + cellSize / 8, y + cellSize / 8, cellSize / 4, cellSize / 4);
}

function DrawPlayers() {
  for(i=1; i<number+1; i++)
  {
    if(!(curPos[i].dead))
    {
      if(i==id)
      {
        ctx.fillStyle = 'blue';
        ctx.fillRect((curPos[i].pos.x+1)*cellSize/2,(curPos[i].pos.y+1)*cellSize/2,  cellSize/2, cellSize/2);
      }
      else
      {
        ctx.fillStyle = 'red';
        ctx.fillRect((curPos[i].pos.x+1)*cellSize/2,(curPos[i].pos.y+1)*cellSize/2,  cellSize/2, cellSize/2);
      }
    }
  }
}
