
  $( "body" ).keydown(function(event) {
  if(dead) return;
    if( event.keyCode == 68 || event.keyCode == 39 )
    {
      moveRight(curPos);
    }
    if( event.keyCode == 83 || event.keyCode == 40 )
    {
      moveDown(curPos);
    }
    if( event.keyCode == 65 || event.keyCode == 37 )
    {
      moveLeft(curPos);
    }
    if( event.keyCode == 87 || event.keyCode == 38 )
    {
      moveUp();
    }
    if( event.keyCode == 32)
    {
      plantBomb();
    }
});

function moveRight() {
  socket.emit('moveRight', id);
}

function moveUp() {
  socket.emit('moveUp', id);
}

function moveDown() {
  socket.emit('moveDown', id);
}

function moveLeft() {
  socket.emit('moveLeft', id);
}

function plantBomb() {
  socket.emit('plantBomb', id);
}
