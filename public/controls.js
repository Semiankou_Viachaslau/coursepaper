socket.on('moveRight', function (curPos) {
  moveRight(curPos);
});

function moveRight(curPos) {
  if(curPos.x<12 && map[curPos.y][curPos.x+1]==0)
  {
    curPos.x++;
    Draw();
  }
}

function moveUp(curPos) {
  if(curPos.y>0 && map[curPos.y-1][curPos.x]==0)
  {
    curPos.y--;
    Draw();
  }
}

function moveDown(curPos) {
  if(curPos.y<12 && map[curPos.y+1][curPos.x]==0)
  {
    curPos.y++;
    Draw();
  }
}

function moveLeft(curPos) {
  if(curPos.x>0 && map[curPos.y][curPos.x-1]==0)
  {
    curPos.x--;
    Draw();
  }
}

function plantBomb(curPos) {
    bombsPos.push({x: curPos.x, y: curPos.y, timePlanted: 0});
    map[curPos.y][curPos.x] = 3;
    Draw();
}
