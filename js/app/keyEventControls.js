
  $( "body" ).keydown(function(event) {
  if(dead) return;
    if( event.keyCode == 68 || event.keyCode == 39 )
    {
        moveRight(curPos1);
    }
    if( event.keyCode == 83 || event.keyCode == 40 )
    {
      moveDown(curPos1);
    }
    if( event.keyCode == 65 || event.keyCode == 37 )
    {
      moveLeft(curPos1);
    }
    if( event.keyCode == 87 || event.keyCode == 38 )
    {
      moveUp(curPos1);
    }
    if( event.keyCode == 32)
    {
      plantBomb(curPos1);
    }
});

function moveRight(curPos) {
  if(curPos1.x<12 && map[curPos1.y][curPos1.x+1]==0)
  {
    curPos.x++;
    Draw();
  }
}

function moveUp(curPos) {
  if(curPos1.y>0 && map[curPos1.y-1][curPos1.x]==0)
  {
    curPos.y--;
    Draw();
  }
}

function moveDown(curPos) {
  if(curPos1.y<12 && map[curPos1.y+1][curPos1.x]==0)
  {
    curPos.y++;
    Draw();
  }
}

function moveLeft(curPos) {
  if(curPos1.x>0 && map[curPos1.y][curPos1.x-1]==0)
  {
    curPos.x--;
    Draw();
  }
}

function plantBomb(curPos) {
    bombsPos.push({x: curPos.x, y: curPos.y, timePlanted: 0});
    map[curPos.y][curPos.x] = 3;
    Draw();
}
