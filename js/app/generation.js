var viewport = {
    width  : $(window).width(),
    height : $(window).height()
};

if( viewport.width <= 1024 )
{
  if(viewport.height > viewport.width)
  {
    cellSize = viewport.width / 9.5;
  }
  else
  {
    cellSize = (viewport.height - 250) / 9.5;
  }
}
else
{
  cellSize = 80;
}

var example = document.getElementById("example"),
ctx = example.getContext('2d');
dead = false;

map = [
      [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
      [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
      [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
      [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
      [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
      [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [ 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0],
      [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      ];
map = CreateBricks(map);

console.log($(".control"));
var curPos1 = {x: 0, y: 0};
bombsPos = [];
boomsPos = [];
example.width = 7.5 * cellSize;
example.height = 7.5 * cellSize;
Draw();
function Draw(){
  ctx.fillStyle = '#ccc';
  ctx.fillRect(0, 0, example.width, example.height);
  ctx.fillStyle = 'green';
  ctx.fillRect(cellSize/2, cellSize/2, 6.5 * cellSize, 6.5 * cellSize)
  for (var j = 0; j < 13; j++)
  for (var i = 0; i < 13; i++) {
    switch (map[j][i]) {
      case 1:
        DrawBrick(i * cellSize / 2 + cellSize/2, j * cellSize / 2 + cellSize/2);
        break;
      case 2:
        DrawHardBrick(i * cellSize / 2 + cellSize/2, j * cellSize / 2 + cellSize/2);
        break;
      }
  }
  DrawPlayer(curPos1);
  DrawBomb(bombsPos);
  DrawBoom(boomsPos);
}

function CreateBricks(a) {
    for( var i=0; i<13; i++ )
    {
        for ( var j=0; j<13; j++ )
        {
            if(!a[i][j])
            {
                var tmp = Math.floor((Math.random() * 3) + 1);
                if(tmp == 2)
                {
                    a[i][j] = 1;
                }
            }
        }
        a[0][0]=a[0][1]=a[1][0]=0;
        a[0][11]=a[0][12]=a[1][12]=0;
        a[11][0]=a[12][0]=a[12][1]=0;
        a[11][12]=a[12][11]=a[12][12]=0;
    }
    return a;
}

function DrawBomb(bombPos) {
  for (i = 0; i < bombPos.length; i++) {
      ctx.beginPath();
      ctx.arc((bombPos[i].x+1+1/2)*cellSize/2, (bombPos[i].y+1+1/2)*cellSize/2, cellSize/4, 0, 4 * Math.PI, false);
      ctx.fillStyle = 'yellow';
      ctx.fill();
      ctx.lineWidth = 5;
  }
}

function DrawBoom(boomPos) {
  for (i = 0; i < boomPos.length; i++) {
    DrawSmallBoom(boomPos[i].x,boomPos[i].y);
    DrawLeftBoom(boomPos[i].x,boomPos[i].y);
    DrawUpBoom(boomPos[i].x,boomPos[i].y);
    DrawDownBoom(boomPos[i].x,boomPos[i].y);
    DrawRightBoom(boomPos[i].x,boomPos[i].y);
  }
}

function DrawLeftBoom(x, y) {
  if(!x) return;
  if(map[y][x-1] == 2){
    return;
  }
  if(map[y][x-1] == 1){
    map[y][x-1] = 0;
    DrawSmallBoom(x-1, y);
    return;
  }
  DrawSmallBoom(x-1, y);
  if(x-1 == 0) return 0;
  if(map[y][x-2] == 2){
    return;
  }
  if(map[y][x-2] == 1){
    map[y][x-2] = 0;
  }
  DrawSmallBoom(x-2, y);
}

function DrawUpBoom(x, y) {
  if(!y) return;
  if(map[y-1][x] == 2){
    return;
  }
  if(map[y-1][x] == 1){
    map[y-1][x] = 0;
    DrawSmallBoom(x, y-1);
    return;
  }
  DrawSmallBoom(x, y-1);
  if(y-1 == 0) return;
  if(map[y-2][x] == 2){
    return;
  }
  if(map[y-2][x] == 1){
    map[y-2][x] = 0;
  }
  DrawSmallBoom(x, y-2);
}

function DrawDownBoom(x, y) {
  if(y == 12) return;
  if(map[y+1][x] == 2 ){
    return;
  }
  if(map[y+1][x] == 1){
    map[y+1][x] = 0;
    DrawSmallBoom(x, y+1);
    return;
  }
  DrawSmallBoom(x, y+1);
  if(y+1 == 12) return;
  if(map[y+2][x] == 2){
    return;
  }
  if(map[y+2][x] == 1){
    map[y+2][x] = 0;
  }
  DrawSmallBoom(x, y+2);
}

function DrawRightBoom(x, y) {
  if( x == 12 ) return;
  if(map[y][x+1] == 2){
    return;
  }
  if(map[y][x+1] == 1){
    map[y][x+1] = 0;
    DrawSmallBoom(x+1, y);
    return;
  }
  DrawSmallBoom(x+1, y);
  if(x+1 == 12) return;
  if(map[y][x+2] == 2){
    return;
  }
  if(map[y][x+2] == 1){
    map[y][x+2] = 0;
  }
  DrawSmallBoom(x+2, y);
}

function DrawSmallBoom(x, y) {
  ctx.beginPath();
  ctx.arc((x+1+1/2)*cellSize/2, (y+1+1/2)*cellSize/2, cellSize/4, 0, 2 * Math.PI, false);
  ctx.fillStyle = 'gray';
  ctx.fill();
  ctx.lineWidth = 5;
  if(dead) return;
  if(curPos1.x == x && curPos1.y == y) Death();
}

function Death()
{
  dead = true;
  curPos1.x = -1;
  curPos1.y = -1;
  alert("You're dead...");
}

function DrawBrick(x, y) {
    ctx.fillStyle = '#FFA500';
    ctx.fillRect(x, y, cellSize / 2, cellSize / 2);
    ctx.fillStyle = '#CD8500';
    ctx.fillRect(x, y, cellSize / 2, cellSize / 16);
    ctx.fillRect(x, y + cellSize / 4, cellSize / 2, cellSize / 16);
    ctx.fillRect(x + cellSize / 4, y, cellSize / 16, cellSize / 4);
    ctx.fillRect(x + cellSize / 16, y + cellSize / 4, cellSize / 16, cellSize / 4);
    ctx.fillStyle = '#D3D3D3';
    ctx.fillRect(x, y + cellSize / 4 - cellSize / 16, cellSize / 2, cellSize / 16);
    ctx.fillRect(x, y + cellSize / 2 - cellSize / 16, cellSize / 2, cellSize / 16);
    ctx.fillRect(x + cellSize / 4 - cellSize / 16, y, cellSize / 16, cellSize / 4);
    ctx.fillRect(x, y + cellSize / 4 - cellSize / 16, cellSize / 16, cellSize / 4);
}

function DrawHardBrick(x, y) {
    ctx.fillStyle = '#cccccc';
    ctx.fillRect(x, y, cellSize / 2, cellSize / 2);
    ctx.fillStyle = '#909090';
    ctx.beginPath();
    ctx.moveTo(x, y + cellSize / 2);
    ctx.lineTo(x + cellSize / 2, y + cellSize / 2);
    ctx.lineTo(x + cellSize / 2, y);
    ctx.fill();
    ctx.fillStyle = '#eeeeee';
    ctx.fillRect(x + cellSize / 8, y + cellSize / 8, cellSize / 4, cellSize / 4);
}

function DrawPlayer(curPos) {
    if(!dead)
    {
      ctx.fillStyle = 'red';
      ctx.fillRect((curPos.x+1)*cellSize/2,(curPos.y+1)*cellSize/2,  cellSize/2, cellSize/2);
    }
}
